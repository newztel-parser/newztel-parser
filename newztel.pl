#!/usr/bin/perl 

# An attempt to machine-parse newztel files to extract meaningful data

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2014 Robin Sheat

# e.g. ~/newztel/newztel.pl */*.txt > info.json

use Modern::Perl;
use Getopt::Long;
use Data::Dumper;
use File::Basename;
use File::Slurp;
use autodie;
use utf8;
use JSON;

my %config;
GetOptions(\%config, );

my @files = @ARGV;

my ($count, $success, $failed) = (0,0,0);
foreach my $file (@files) {
    $count++;
    warn "File: $file\n";
    open(my $fh, '<', $file);
    my $content = read_file($fh);
    close($fh);
    my %res;
    eval {
        %res = parse($content, basename($file));
    };
    if ($@) {
        warn "Failed: $@\n";
        die if $@ =~ /reallydie/;
        $failed++;
        print to_json({ filename=>$file, failed => 1 }, {pretty=>1}) . "\n";
    } else {
        $success++;
        print to_json({ filename=>$file, %res, failed => 0 }, {pretty=>1}) . "\n";
    }
}
warn "Files analysed: $count\nFiles processed: $success\nFiles failed: $failed\n";

sub parse {
    my ($content, $id) = @_;

    my %res;

    my %content = lineify($content);

    # Try to extract the title data. This tends to include the date, maybe
    # the time, and the show title.
    my %title = get_title(%content);
    my %body = get_body(%content);
    my $date = convert_date(%title);
    return (
        date => $date,
        titledata => \%title,
        body => \%body,
    );
}

sub get_title {
    my (%content) = @_;

    # Grab the first line
    my $line = $content{title};

    # Dates always seem to be of the form:
    # MONDAY 31 FEBRUARY 1975
    # but can be anywhere in the string
    my $dow_re = '(?<dow>sunday|monday|tuesday|wednesday|thursday|friday|saturday|sunday)';
    my $dom_re = '(?<dom>[0-9]?[1-9])';
    my $month_re = '(?<month>january|february|march|april|may|june|july|august|september|october|november|december)';
    my $year_re = '(?<year>[12][90][8901][0-9])';

    my $date_re = qr#$dow_re.*?$dom_re.*?$month_re.*?$year_re#i;

    $line =~ /$date_re/i;
    my %date = (
        dow => $+{dow},
        dom => $+{dom},
        month => $+{month},
        year => $+{year},
    );
    if (! defined($+{dow})) {
        # try another ordering
        $date_re = qr#$dow_re.*?$month_re.*?$dom_re.*?$year_re#i;
        $line =~ /$date_re/i;
        %date = (
            dow => $+{dow},
            dom => $+{dom},
            month => $+{month},
            year => $+{year},
        );
    }
    die "Unable to extract date from:\n$line\nContent:\n".Dumper(\%content) unless $date{dow};

    my %title;
    # Remove the date and extract the title.
    $line =~ s/\s*$date_re:?\s*//;

    # The title /may/ have leaders, or it may not.
    # It may sometimes start with a *
    $line =~ s/^\*//;
    $line =~ s/^.*?(?:NEWZTEL )?(?:LOGS?|NEWS)[;:]\s*//;
    $line =~ m/LOG:(?<showname>.*?):/;
    $title{showname} = $+{showname} if $+{showname};
    $line =~ s/LOG:.*?:+\s*//;
    $title{title} = $line;
    die "reallydie:\n".Dumper(\%title, $line) if $line =~ /LOG/;

    return ( %date, %title );
}

sub get_body {
    my (%content) = @_;

    return ( content => $content{body} );
}
# This attempts to convert the input into real lines and parts, as much as
# possible.
sub lineify {
    my ($content) = @_;

    my @broken = split /(?:\r\n|\n)/, $content;
    my $type = guess_type(@broken);
    my $state = 'start';
    my ($temp, %output);
    foreach my $br (@broken, '') {
        my $blank = ($br =~ /^\s*$/);
        if ($state eq 'start') {
            if ($type eq 'mail') {
                # Clear out the headers
                if ($blank) {
                    $type = 'standard';
                }
            } elsif ($type eq 'newztel-header') {
                # Clear out title thing. Note this format may contain many records batched together. I don't handle that yet.
                if ($blank) {
                    $type = 'standard';
                }
            } elsif ($br =~ /.{5}/) {
                # Make sure there is actually some stuff in the title row
                $temp = $br;
                $state = 'title';
            }
        } elsif ($state eq 'title') {
            if ($blank) {
                $output{title} = $temp;
                undef $temp;
                $state = 'body';
            } elsif ($br =~ /^\d\d\d\d\s*$/) {
                # Date sometimes pushed to next line
                $temp .= ' ' . $br;
            } elsif ($br =~ /^\w/) {
                $output{title} = $temp;
                $temp = $br;
                $state = 'body';
            } else {
                $br =~ s/^\s*(.*)\s*$/$1/;
                $temp .= $br;
            }
        } elsif ($state eq 'body') {
            if (!$blank) {
                if ($br eq 'ENDS') {
                    $state = 'footer';
                    push @{ $output{body} }, $temp;
                    undef $temp;
                } else {
                    $temp = $temp ? "$temp $br" : $br;
                }
            } else {
                if ($temp) {
                    push @{ $output{body} }, $temp;
                    undef $temp;
                }
            }
        }
    }
    return %output;
}

sub guess_type {
    my (@lines) = @_;

    die "Unable to guess type" if !@lines;
    my $c = @lines > 10 ? 10 : @lines;
    my $start = join("\n", grep { defined($_) } @lines[0 .. $c]);
    return 'mail' if ($start =~ /^(Received:|From:)/);
    return 'newztel-header' if ($lines[0] =~ /NEWZTEL MONITORING SERVICE/);
    return 'plain';
}

sub convert_date {
    my (%title) = @_;
    my %month_conv = (
        january => 1,
        february => 2,
        march => 3,
        april => 4,
        may => 5,
        june => 6,
        july => 7,
        august => 8,
        september => 9,
        october => 10,
        november => 11,
        december =>12,
    );

    my $month_num = $month_conv{lc $title{month} };
    die "reallydie: month didn't parse: ".Dumper(\%title, $month_num) unless $month_num;
    my $date = $title{year} . '-' . sprintf('%02s', $month_num) . '-' . sprintf('%02s', $title{dom});
}
